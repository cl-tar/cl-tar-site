;;; This file is part of cl-tar-site. See LICENSE and README.md for more
;;; information.

(uiop:define-package #:cl-tar-site
    (:use #:cl
          #:40ants-doc))
